import React, { useState } from "react";
import "./App.css";
import Login from "./components/Login/Login";
import { Route, Routes } from "react-router-dom";
import BlogListing from "./components/BlogListing/BlogListing";
import BlogPage from "./components/BlogListing/BlogPage";
import { phoneValidation } from "./components/helper";

function App() {
  return (
    <Routes>
      <Route exact path="blog" element={<BlogListing />} />
      <Route exact path="blogPage" element={<BlogPage />} />
      <Route exact path="/" element={<Login />} />
    </Routes>
  );
}

export default App;
