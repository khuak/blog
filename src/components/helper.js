export const RemoveUnicode =(string)=>{
    console.log(string);
    if (string.indexOf("&#8217;") >= 0) {
        return this.RemoveUnicode(string.replace("&#8217;", "'"));
      } else {
        return string.replace("<p>", "").replace("[&hellip;]</p>", "...");
      }
}

export const phoneValidation = (phoneNumber) => {
  const regex = /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/i;
  return !(!phoneNumber || regex.test(phoneNumber) === false);
};