import React from "react";
import "./BlogView.css";

const BlogView = (props) => {
  if (props.post) {
    return (
      <>
        <div className="article">
          <h1 className="text-center">{props.post.title}</h1>
          <div className="content">{props.post.body}</div>
        </div>
      </>
    );
  }
};

export default BlogView;
