import React, { useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import axios from "axios";
import BlogView from "./BlogView";
import "./BlogListing.css";

const BlogListing = (props) => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        setPosts(res.data);
      })
      .catch((error) => console.log(error));
  }, []);
  return (
    <div className="blog">
      <h1>Articles</h1>
      {posts.map((post) => {
        return <BlogView post={post} />;
      })}
    </div>
  );
};

export default BlogListing;
