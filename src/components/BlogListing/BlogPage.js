import React, { useEffect, useState } from "react";
import axios from "axios";
// import { useHistory } from "react-router-dom";

const BlogPage = () => {
  const [post, setPost] = useState([]);
  useEffect(() => {
    const id = 1;
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        console.log(res.data);
        setPost(res.data);
      })
      .catch((error) => console.log(error));
  });
  return (
    <div className="article">
      <h1 className="text-center">{post.title}</h1>
      <div className="content">{post.body}</div>
    </div>
  );
};

export default BlogPage;
