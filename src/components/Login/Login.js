import React, { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { phoneValidation } from "../helper";

import "./Login.css";

const Login = (props) => {
  const phoneNumbers = ["8237497582", "9967045306"];
  const [displayOtp, setDisplayOtp] = useState(false);
  const [showOtpBtn, setShowOtpBtn] = useState(true);
  const [isauhtenticated, setIsauhtenticated] = useState(
    localStorage.getItem(localStorage.getItem("authenticated") || false)
  );
  const [phoneNumber, setPhoneNumber] = useState("");
  const [message, setMessage] = useState("");
  const navigate = useNavigate();

  const sendOtp = () => {
    if(!phoneNumber){
      setDisplayOtp(false);
      setShowOtpBtn(true);
      setMessage("Phone Number not valid");
    }else{
      setDisplayOtp(true);
      setShowOtpBtn(false);
    }
  };

  const changeHandler = (e) => {
    setPhoneNumber(e.target.value);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    const isPhoneValid = phoneValidation(phoneNumber);
    const account = phoneNumbers.find((number) => number === phoneNumber);
    if (!isPhoneValid) {
      setMessage("Phone Number not valid");
      // setShowOtpBtn(false);
    } else if (!account) {
      setMessage("User dont have account for this number");
    } else {
      setIsauhtenticated(true);
      localStorage.setItem("authenticated", true);
      navigate("/blog");
    }
  };

  return (
    <div className="wrapper">
      <div class="login-form">
        <h1>Login</h1>
        <div className="error">{message}</div>
        {isauhtenticated && <Navigate to="/blog" replace={true} />}
        <div className="content">
          <div className="input-field">
            <input
              type="number"
              placeholder="Mobile Number"
              onChange={changeHandler}
            />
          </div>
        </div>
        <div className="content">
          <div className="input-field">
            <input
              type="number"
              style={{ display: displayOtp === true ? "block" : "none" }}
              placeholder="OTP"
            />
          </div>
        </div>
        {showOtpBtn === true ? (
          <div className="action">
            <button type="button" onClick={sendOtp}>
              Send OTP
            </button>
          </div>
        ) : (
          <div className="action">
            <button onClick={submitHandler}>Sign in</button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Login;
